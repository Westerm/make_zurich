/*
 * 1. use app EUI and app key from TTN and set it in the module
 * 2. join the network using OTAA
 */

#include "loramodem.h"
#define WATER_SENSOR 4

LoRaWANModem modem;

const uint8_t appeui[8]  = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x03, 0x71, 0xD4 };
const uint8_t appkey[16] = { 0x02, 0x92, 0xBC, 0x21, 0x6E, 0xAF, 0x3F, 0x26, 0x5E, 0xBE, 0xB7, 0xCA, 0x74, 0x19, 0x3C, 0xF7 };

void setup() {
  Serial.begin(9600);
  while (!Serial);

  modem.begin();
  modem.info();
  modem.join(appeui, appkey);

  unsigned long current_time = millis();
  while (modem.is_joining()) {
    if ((millis()-current_time) > 1000) {
      current_time = millis();
      Serial.println("waiting ...");
    }
  }
  Serial.println("joined");

  pinMode(WATER_SENSOR, INPUT);

}

void loop() {
  delay(10000);
  Serial.println("sending");
  Serial.println(digitalRead(WATER_SENSOR));
  uint8_t payload[11] = { 0x6d, 0x61, 0x6b, 0x65, 0x20, 0x7a, 0x75, 0x72, 0x69, 0x63, 0x68 };
  modem.send(payload, 11);
}
