# node.py
# Simple Python client to show node activity from ttn MQTT brooker with credentials
# Credits: R.Schimmel / www.schimmel-bisolutions.nl
# 
# 

import paho.mqtt.client as mqtt
import json
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

# App Reference Data
appID   = "pf_forgranny_app_1"
appEUI  = "70B3D57ED00371D4"
appKey  = "ttn-account-v2.P-R2sTFTmBIZFJE9yOhv6qmMI1nrQSVGEsj8nyBLImY"

# Smart Meter Data
filePath    = r"C:\Users\User\Documents\Documents\MakeZurich2020\GitLab\SmartMeter_data\2012-10-24.csv"
smartData   = pd.read_csv(filePath,header=None) # 1 Hz / column 1: electricity total building
index       = pd.date_range('31/10/2020', periods=len(smartData), freq='S')
smartData.index = index


#
df      = pd.DataFrame(columns=['Time','Water'])

# Call back functions 
# gives connection message
def on_connect(client,userdata,flags,rc):
    print("Connected with result code:"+str(rc))
    # subscribe for all devices of user
    client.subscribe('+/devices/+/up')

# gives message from device
def on_message(client,userdata,msg):    
    # print("Topic",msg.topic + "\nMessage:" + str(msg.payload))
    data = json.loads(msg.payload)
    
    time    = datetime.strptime(data["metadata"]["time"].split("T")[0] + " " +data["metadata"]["time"].split("T")[1].split(r".")[0],'%Y-%m-%d %H:%M:%S')
    payload = data["payload_fields"]["field1"]
    
    print(time,payload)
    
    # store data
    localDF  = pd.DataFrame([[time,payload]],columns=['Time','Water'])
    global df
    df = df.append(localDF)
    
    # Plot data
    fig, ax1 = plt.subplots()
    ax1.plot(df['Time'],df['Water'],color='blue')
    ax2 = ax1.twinx()
    #
    start   = np.where(smartData.index==df['Time'].iloc[0])[0][0]
    end     = np.where(smartData.index==df['Time'].iloc[-1])[0][0]
    ax2.plot(smartData[0].iloc[start:end],color='orange')
    
    # ax1.legend(['Water','Electricity'])
    ax1.set_xlabel('Time')
    ax1.set_ylabel('Water')
    ax2.set_ylabel('Electricity')
    plt.show()
    
    return df
    
def on_log(client,userdata,level,buf):
    print("message:" + str(buf))
    print("userdata:" + str(userdata))
    
mqttc               = mqtt.Client()
mqttc.on_connect    = on_connect
mqttc.on_message    = on_message
# mqttc.on_log        = on_log

mqttc.username_pw_set(appID,appKey)

mqttc.connect("eu.thethings.network", 1883, 10)

# and listen to server
run = True
i = 0
while run:
    mqttc.loop()
    
    i = i + 1